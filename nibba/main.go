package main

import (
	"context"
	"fmt"
	"log"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
)

func main() {
	client, err := ethclient.Dial("http://localhost:8545")
	if err != nil {
		log.Fatal(err)
	}

	privateKey, err := crypto.HexToECDSA("4e3ab1cc5c9d4ca2d4175aa986916eddc985f050d9b7e9c80930252a88f08dc7")
	if err != nil {
		log.Fatal(err)
	}

	auth := bind.NewKeyedTransactor(privateKey)

	address, tx, greter, err := DeployGreeter(auth, client, "wassup, nigga?")
	if err != nil {
		log.Fatalf("Failed to deploy new token contract: %v", err)
	}

	fmt.Printf("Contract pending deploy: 0x%x\n", address)
	fmt.Printf("Transaction waiting to be mined: 0x%x\n\n", tx.Hash())

	ctx := context.Background()
	_, err = bind.WaitDeployed(ctx, client, tx)
	if err != nil {
		log.Fatalf("failed to deploy contact when mining :%v", err)
	}

	tx, err = greter.Change(auth, "white pride")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Wait mining")
	ctx = context.Background()
	bind.WaitMined(ctx, client, tx)

	greeting, err := greter.Greet(
		&bind.CallOpts{
			Pending: true,
		},
	)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Greeting %s\n", greeting)
}
