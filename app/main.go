package main

import (
	"context"
	"crypto/ecdsa"
	"fmt"
	"log"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"

	"../contracts"
)

func main() {
	client, err := ethclient.Dial(ethnet)
	if err != nil {
		log.Fatal(err)
	}

	privateKey, err := crypto.HexToECDSA(secret)
	if err != nil {
		log.Fatal(err)
	}

	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		log.Fatal("error casting public key to ECDSA")
	}

	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	nonce, err := client.PendingNonceAt(context.Background(), fromAddress)
	if err != nil {
		log.Fatal(err)
	}

	//opts := bind.NewKeyedTransactor(privateKey)

	gasPrice, err := client.SuggestGasPrice(context.Background())
	if err != nil {
		log.Fatal(err)
	}

	_, _ = nonce, gasPrice

	auth := bind.NewKeyedTransactor(privateKey)
	//auth.Nonce = big.NewInt(int64(nonce))
	//auth.Value = big.NewInt(0)     // in wei
	//auth.GasLimit = uint64(300000) // in units
	//auth.GasPrice = gasPrice

	address, tx, instance, err := contracts.DeployBallot(auth, client)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("waiting for contract to get mined")
	ctx := context.Background()
	addressAfterMined, err := bind.WaitDeployed(ctx, client, tx)
	if err != nil {
		log.Fatalf("failed to deploy contact when mining :%v", err)
	}

	if _, err := instance.Set(auth, "hello"); err != nil {
		log.Fatal(err)
	}

	sth, err := instance.Get(&bind.CallOpts{
		Pending: true,
	})

	fmt.Println("ballot.penis: ", sth, err)

	_ = address
	_ = addressAfterMined
	_ = instance
}
